unsigned long shfr(unsigned long x, unsigned long n) { return (x >> n); }
unsigned long rotr(unsigned long x, unsigned long n) { return ((x >> n) | (x << ((sizeof(x) << 3) - n))); }
unsigned long rotl(unsigned long x, unsigned long n) { return ((x << n) | (x >> ((sizeof(x) << 3) - n))); }
unsigned long CH(unsigned long x, unsigned long y, unsigned long z) { return ((x & y) ^ (~x & z)); }
unsigned long MAJ(unsigned long x, unsigned long y, unsigned long z) { return ((x & y) ^ (x & z) ^ (y & z)); }
unsigned long F1(unsigned long x) { return (rotr(x, 28) ^ rotr(x, 34) ^ rotr(x, 39)); }
unsigned long F2(unsigned long x) { return (rotr(x, 14) ^ rotr(x, 18) ^ rotr(x, 41)); }
unsigned long F3(unsigned long x) { return (rotr(x, 1) ^ rotr(x, 8) ^ shfr(x, 7)); }
unsigned long F4(unsigned long x) { return (rotr(x, 19) ^ rotr(x, 61) ^ shfr(x, 6)); }


void rounds(unsigned long *tab, unsigned long *h, unsigned long *k) {
	unsigned long t1, t2;
	unsigned long w[80];
	unsigned long wv[8];
	unsigned int i, j;
	for (i = 0; i < 16; i++) w[i] = tab[i];
	for (i = 16; i < 80; i++) w[i] = F4(w[i - 2]) + w[i - 7] + F3(w[i - 15]) + w[i - 16];
	for (j = 0; j < 8; j++) wv[j] = h[j];
	for (j = 0; j < 80; j++) {
		t1 = wv[7] + F2(wv[4]) + CH(wv[4], wv[5], wv[6])
			+ k[j] + w[j];
		t2 = F1(wv[0]) + MAJ(wv[0], wv[1], wv[2]);
		wv[7] = wv[6];
		wv[6] = wv[5];
		wv[5] = wv[4];
		wv[4] = wv[3] + t1;
		wv[3] = wv[2];
		wv[2] = wv[1];
		wv[1] = wv[0];
		wv[0] = t1 + t2;
	}
	for (j = 0; j < 8; j++) h[j] += wv[j];
}



void sha512(unsigned long *tab, unsigned long size, unsigned long *h) {
	unsigned long k[80] =
	{
		0x428a2f98d728ae22, 0x7137449123ef65cd,
		0xb5c0fbcfec4d3b2f, 0xe9b5dba58189dbbc,
		0x3956c25bf348b538, 0x59f111f1b605d019,
		0x923f82a4af194f9b, 0xab1c5ed5da6d8118,
		0xd807aa98a3030242, 0x12835b0145706fbe,
		0x243185be4ee4b28c, 0x550c7dc3d5ffb4e2,
		0x72be5d74f27b896f, 0x80deb1fe3b1696b1,
		0x9bdc06a725c71235, 0xc19bf174cf692694,
		0xe49b69c19ef14ad2, 0xefbe4786384f25e3,
		0x0fc19dc68b8cd5b5, 0x240ca1cc77ac9c65,
		0x2de92c6f592b0275, 0x4a7484aa6ea6e483,
		0x5cb0a9dcbd41fbd4, 0x76f988da831153b5,
		0x983e5152ee66dfab, 0xa831c66d2db43210,
		0xb00327c898fb213f, 0xbf597fc7beef0ee4,
		0xc6e00bf33da88fc2, 0xd5a79147930aa725,
		0x06ca6351e003826f, 0x142929670a0e6e70,
		0x27b70a8546d22ffc, 0x2e1b21385c26c926,
		0x4d2c6dfc5ac42aed, 0x53380d139d95b3df,
		0x650a73548baf63de, 0x766a0abb3c77b2a8,
		0x81c2c92e47edaee6, 0x92722c851482353b,
		0xa2bfe8a14cf10364, 0xa81a664bbc423001,
		0xc24b8b70d0f89791, 0xc76c51a30654be30,
		0xd192e819d6ef5218, 0xd69906245565a910,
		0xf40e35855771202a, 0x106aa07032bbd1b8,
		0x19a4c116b8d2d0c8, 0x1e376c085141ab53,
		0x2748774cdf8eeb99, 0x34b0bcb5e19b48a8,
		0x391c0cb3c5c95a63, 0x4ed8aa4ae3418acb,
		0x5b9cca4f7763e373, 0x682e6ff3d6b2b8a3,
		0x748f82ee5defb2fc, 0x78a5636f43172f60,
		0x84c87814a1f0ab72, 0x8cc702081a6439ec,
		0x90befffa23631e28, 0xa4506cebde82bde9,
		0xbef9a3f7b2c67915, 0xc67178f2e372532b,
		0xca273eceea26619c, 0xd186b8c721c0c207,
		0xeada7dd6cde0eb1e, 0xf57d4f7fee6ed178,
		0x06f067aa72176fba, 0x0a637dc5a2c898a6,
		0x113f9804bef90dae, 0x1b710b35131c471b,
		0x28db77f523047d84, 0x32caab7b40c72493,
		0x3c9ebe0a15c9bebc, 0x431d67c49c100d4c,
		0x4cc5d4becb3e42b6, 0x597f299cfc657e2a,
		0x5fcb6fab3ad6faec, 0x6c44198c4a475817
	};
	unsigned long tab1[16];
	unsigned long tab2[16];
	h[0] = 7640891576956012808;
	h[1] = 13503953896175478587;
	h[2] = 4354685564936845355;
	h[3] = 11912009170470909681;
	h[4] = 5840696475078001361;
	h[5] = 11170449401992604703;
	h[6] = 2270897969802886507;
	h[7] = 6620516959819538809;
	unsigned int j, i, z, bitter;
	unsigned int nblock = size / 128;
	unsigned int lastblock = size % 128;

	for (j = 0; j < nblock; j++) {
		for (i = 0; i < 16; i++) tab1[i] = tab[i + j * 16];
		rounds(tab1, h, k);

	}

	for (i = 0; i < 16; i++) tab1[i] = 0;

	for (i = 0; i < 15; i++) tab2[i] = 0;
	tab2[15] = size * 8;
	z = lastblock % 8;
	unsigned int tmp;
	if (z == 0) {
		if (size == 128) {
			tab2[0] = 0x8000000000000000UL;
			rounds(tab2, h, k);
		}
		else {
			tmp = (lastblock / 8);
			for (i = 0; i < tmp; i++) tab1[i] = tab[nblock * 16 + i];
			tab1[tmp] = 0x8000000000000000UL;
			if (tmp < 14) tab1[15] = size * 8;
			rounds(tab1, h, k);
			if (tmp >= 14) {
				rounds(tab2, h, k);
			}
		}
	}
	else {
		tmp = (lastblock / 8) + 1;
		for (i = 0; i < tmp; i++) tab1[i] = tab[nblock * 16 + i];
		if (tmp < 14) {
			tab1[tmp - 1] ^= 0x80 << ((8 - z) * 8);
			tab1[15] = size * 8;
			rounds(tab1, h, k);
		}

	}

}


void hmac(unsigned long *key, unsigned long *message, unsigned int messageLength, unsigned int keylength, unsigned long* finalout)
{

	unsigned int i = 0;
	const unsigned int blocksize = 16;
	unsigned long o_pad = 0x5c5c5c5c5c5c5c5c;
	unsigned long i_pad = 0x3636363636363636;

	if (keylength > blocksize)
		sha512(key, keylength * 8, key);

	for (i = keylength; i < blocksize; i++)
	{
		key[i] = 0x0000000000000000;
	}

	unsigned long o_key_pad[16];
	for (i = 0; i < blocksize; i++)
	{
		o_key_pad[i] = o_pad ^ key[i];
	}

	unsigned long i_key_pad[16];
	for (i = 0; i < blocksize; i++)
		i_key_pad[i] = i_pad ^ key[i];

	unsigned long block[24];
	for (i = 0; i < blocksize; i++)
		block[i] = i_key_pad[i];
	for (i = 0; i < messageLength / 8; i++)
		block[i + blocksize] = message[i];

	unsigned long output1[8];
	sha512(block, (blocksize + messageLength / 8) * 8, output1);

	unsigned long block2[24];
	for (i = 0; i < blocksize; i++)
		block2[i] = o_key_pad[i];
	for (i = 0; i < 8; i++)
		block2[i + blocksize] = output1[i];

	sha512(block2, 24 * 8, finalout);
}

void F(unsigned long *password, unsigned long *salt, unsigned long saltLen, unsigned int count, unsigned int index, unsigned long passLen,  unsigned long *Fout)
{
	unsigned int saltLenUL = (saltLen - 1) / 8 + 1;

	unsigned long wejscie[8], wyjscie[8], saltCopy[17], tmp1[8];
	unsigned int i, j;

	for (i = 0; i < saltLenUL; i++)
		saltCopy[i] = salt[i];

	unsigned long tmp = (saltLen) % 8;

	if (tmp <= 4 && tmp != 0)
		saltCopy[saltLenUL - 1] ^= index << ((4 - tmp) * 8);
	else if (tmp == 0) {
		tmp = index;
		saltCopy[saltLenUL] = tmp << 32;
		saltLenUL++;
	}
	else
	{
		unsigned long tmp2, tmp3;
		tmp2 = index >> (tmp - 4) * 8;
		unsigned long reszta = index - (tmp2 << ((tmp - 4) * 8));
		saltCopy[saltLenUL - 1] ^= tmp2;
		saltCopy[saltLenUL] = reszta << 64 - 8 * (tmp - 4);
		saltLenUL++;
	}


	hmac(password, saltCopy, saltLenUL * 8, passLen, wyjscie);
	for (i = 0; i < 8; i++) {
		Fout[i] = 0;
		tmp1[i] = wyjscie[i];
	}



	for (i = 0; i < 8; i++)
		Fout[i] ^= wyjscie[i];


	for (j = 1; j < count; j++)
	{

		hmac(password, tmp1, 8 * 8, passLen, wyjscie);


		for (i = 0; i < 8; i++) {
			Fout[i] ^= wyjscie[i];
			tmp1[i] = wyjscie[i];
		}


	}


}

void pbkdf2(unsigned long dkLen, unsigned long *password, unsigned long passLen, unsigned long *salt, unsigned long saltLen, unsigned int count, unsigned long *T)
{
	unsigned long hLen = 64; 
	unsigned int l, r, index, j, s, v;
	unsigned long Fout[8];
	unsigned long Tl8[8][8];


	if (dkLen >(4294967295 * hLen))
		printf("ZA DŁUGI DK!");
	l = (dkLen / hLen) + 1;
	r = dkLen % hLen;
	

	for (index = 1; index <= 1; index++)
	{
		F(password, salt, saltLen, count, index, passLen, Fout);
		for (j = 0; j < 8; j++)
			Tl8[index][j] = Fout[j];
	}

	for (s = 0; s < l; s++)
	{
		for (v = 1; v <= l; v++)
		{
			for (j = 0; j < 8; j++)
				T[s + j] = Tl8[v][j];
		}
		s = s + 8;
	}
	int i;

}

int isValid(unsigned long *localOutput, unsigned long *expectedOutput, int outputLength) {
	for (int i = 0; i < outputLength; i++) {
		if (localOutput[i] != expectedOutput[i]) return 0;
	}
	return 1;
}

unsigned long toULTable(unsigned char *input, int inputLength, unsigned long *result) {
	int completeEights = inputLength / 8,
		remainingChars = inputLength % 8;

	int outputLength = completeEights;
	if (remainingChars != 0) outputLength++;

	int i, k = 0;
	for (i = 0; i < completeEights; i++) {
		result[i] = 0;
		for (int j = 0; j < 8; j++) {
			result[i] = result[i] << 8;
			result[i] += input[k];
			k++;
		}
	}
	if (i < 8) {
		result[i] = 0;
		for (int j = 0; j < remainingChars; j++) {
			result[i] = result[i] << 8;
			result[i] += input[k];
			k++;
		}
		for (int j = 0; j < 8 - remainingChars; j++) result[i] = result[i] << 8;
		i++;
		for (int j = i; j < 8; j++) result[j] = 0;
	}
	return outputLength;
}

__kernel void concat_compare(
	int expectedOutputSize,
	__global unsigned long *expectedOutput, int iterations, __global unsigned long *initVector, int initVectorSize,
	__global const unsigned char *kernelBaseTable, int kernelBaseTableSize, __global const unsigned char *globalBase, int globalBaseSize,
	__global const unsigned char *platformBase, int platformBaseSize, __global const unsigned char *deviceBase, int deviceBaseSize,
	__global unsigned long *k, int passwordLength,
	__global int *flag, __global unsigned char *passwords
) {
	int                 globalID = get_global_id(0),
		localOffset = 0,
		fl;
	unsigned char       password[64];
	unsigned long  passwordUL[8],
		pbkdfOutput[8],
		expected[8],
		initV[16],
		passwordLengthUL;


	for (int i = 0; i < 8; i++) {
		expected[i] = expectedOutput[i];
	}
	for (int i = 0; i < 16; i++) {
		initV[i] = initVector[i];
	}

	for (int i = 0; i < globalBaseSize; i++) password[localOffset + i] = globalBase[i];
	localOffset += globalBaseSize;
	for (int i = 0; i < platformBaseSize; i++) password[localOffset + i] = platformBase[i];
	localOffset += platformBaseSize;
	for (int i = 0; i < deviceBaseSize; i++) password[localOffset + i] = deviceBase[i];
	localOffset += deviceBaseSize;
	password[localOffset] = kernelBaseTable[2 * globalID];
	password[localOffset + 1] = kernelBaseTable[(2 * globalID) + 1];

	passwordLengthUL = toULTable(password, passwordLength, passwordUL);



	pbkdf2(expectedOutputSize * 64, passwordUL, passwordLengthUL, initV, initVectorSize * 8, iterations, pbkdfOutput);

	if (globalID == 0) {
		for (int i = 0; i < 8; i++) printf("%c", password[i]);
		printf("\n");
		for (int i = 0; i < 8; i++) printf("%016llx", expected[i]);
		printf("\n");
		for (int i = 0; i < 8; i++) printf("%016llx", pbkdfOutput[i]);
		printf("\n"); printf("\n"); printf("\n");
	}

	fl = isValid(pbkdfOutput, expected, expectedOutputSize);

	flag[globalID] = fl;
	if (fl == 1) {
		int offset = globalID * passwordLength;
		for (int i = 0; i < passwordLength; i++) passwords[offset + i] = password[i];
	}
}