#pragma region Includes
#include <iostream>
#include <string>
#include <fstream>
#include <vector>

#ifdef __APPLE__
#include <OpenCL/opencl.h>
#else
#include <CL/cl.h>
#endif

using namespace std;

 cl_ulong k[80] =
{
	0x428a2f98d728ae22ULL, 0x7137449123ef65cdULL,
	0xb5c0fbcfec4d3b2fULL, 0xe9b5dba58189dbbcULL,
	0x3956c25bf348b538ULL, 0x59f111f1b605d019ULL,
	0x923f82a4af194f9bULL, 0xab1c5ed5da6d8118ULL,
	0xd807aa98a3030242ULL, 0x12835b0145706fbeULL,
	0x243185be4ee4b28cULL, 0x550c7dc3d5ffb4e2ULL,
	0x72be5d74f27b896fULL, 0x80deb1fe3b1696b1ULL,
	0x9bdc06a725c71235ULL, 0xc19bf174cf692694ULL,
	0xe49b69c19ef14ad2ULL, 0xefbe4786384f25e3ULL,
	0x0fc19dc68b8cd5b5ULL, 0x240ca1cc77ac9c65ULL,
	0x2de92c6f592b0275ULL, 0x4a7484aa6ea6e483ULL,
	0x5cb0a9dcbd41fbd4ULL, 0x76f988da831153b5ULL,
	0x983e5152ee66dfabULL, 0xa831c66d2db43210ULL,
	0xb00327c898fb213fULL, 0xbf597fc7beef0ee4ULL,
	0xc6e00bf33da88fc2ULL, 0xd5a79147930aa725ULL,
	0x06ca6351e003826fULL, 0x142929670a0e6e70ULL,
	0x27b70a8546d22ffcULL, 0x2e1b21385c26c926ULL,
	0x4d2c6dfc5ac42aedULL, 0x53380d139d95b3dfULL,
	0x650a73548baf63deULL, 0x766a0abb3c77b2a8ULL,
	0x81c2c92e47edaee6ULL, 0x92722c851482353bULL,
	0xa2bfe8a14cf10364ULL, 0xa81a664bbc423001ULL,
	0xc24b8b70d0f89791ULL, 0xc76c51a30654be30ULL,
	0xd192e819d6ef5218ULL, 0xd69906245565a910ULL,
	0xf40e35855771202aULL, 0x106aa07032bbd1b8ULL,
	0x19a4c116b8d2d0c8ULL, 0x1e376c085141ab53ULL,
	0x2748774cdf8eeb99ULL, 0x34b0bcb5e19b48a8ULL,
	0x391c0cb3c5c95a63ULL, 0x4ed8aa4ae3418acbULL,
	0x5b9cca4f7763e373ULL, 0x682e6ff3d6b2b8a3ULL,
	0x748f82ee5defb2fcULL, 0x78a5636f43172f60ULL,
	0x84c87814a1f0ab72ULL, 0x8cc702081a6439ecULL,
	0x90befffa23631e28ULL, 0xa4506cebde82bde9ULL,
	0xbef9a3f7b2c67915ULL, 0xc67178f2e372532bULL,
	0xca273eceea26619cULL, 0xd186b8c721c0c207ULL,
	0xeada7dd6cde0eb1eULL, 0xf57d4f7fee6ed178ULL,
	0x06f067aa72176fbaULL, 0x0a637dc5a2c898a6ULL,
	0x113f9804bef90daeULL, 0x1b710b35131c471bULL,
	0x28db77f523047d84ULL, 0x32caab7b40c72493ULL,
	0x3c9ebe0a15c9bebcULL, 0x431d67c49c100d4cULL,
	0x4cc5d4becb3e42b6ULL, 0x597f299cfc657e2aULL,
	0x5fcb6fab3ad6faecULL, 0x6c44198c4a475817ULL
};
#pragma endregion

#pragma region ExternalFunctions
//program input processing
string toLowerHexString(char *input) {
	string result = "";
	int i = 0;
	while (input[i]) {
		unsigned char c = tolower(input[i]);
		if (!(c >= '0' && c <= '9')) {
			if (!(c >= 'a' && c <= 'f')) return "";
		}
		result += c;
		i++;
	}
	return result;
}
 cl_ulong* toULLTab(string input,  cl_ulong tabSize) {
	 cl_ulong *result = new  cl_ulong[tabSize];
	int i = 0;
	for (int t = 0; t < tabSize; t++) {
		result[t] = 0;
		for (int x = 0; x < 16; x++) {
			result[t] = result[t] << 4;
			if (input[i] <= '9') result[t] += (input[i] - '0');
			else result[t] += (input[i] - 'a' + 10);
			i++;
		}
	}
	return result;
}
int toInt(char *input) {
	int result = 0, i = 0, tmp;
	while (input[i]) {
		unsigned char c = input[i];
		switch (c) {
		case '0': tmp = 0; break;
		case '1': tmp = 1; break;
		case '2': tmp = 2; break;
		case '3': tmp = 3; break;
		case '4': tmp = 4; break;
		case '5': tmp = 5; break;
		case '6': tmp = 6; break;
		case '7': tmp = 7; break;
		case '8': tmp = 8; break;
		case '9': tmp = 9; break;
		default: return -1;
		}
		result *= 10;
		result += tmp;
		i++;
	}
	return result;
}
void defineAlphabet(string &alphabet, char *input) {
	bool check[4] = { false, false, false, false };
	int j = 0;
	while (input[j]) {
		unsigned char c = input[j];
		switch (c) {
		case 'A':
			if (!check[0]) {
				for (int i = 0; i<26; i++) alphabet += 'A' + i;
				check[0] = true;
			}
			break;
		case 'a':
			if (!check[1]) {
				for (int i = 0; i < 26; i++) alphabet += 'a' + i;
				check[1] = true;
			}
			break;
		case '1':
			if (!check[2]) {
				for (int i = 0; i < 10; i++) alphabet += '0' + i;
				check[2] = true;
			}
			break;
		case '*':
			if (!check[3]) {
				for (int i = 0; i < 16; i++) alphabet += ' ' + i;
				for (int i = 0; i < 7; i++) alphabet += ':' + i;
				for (int i = 0; i < 6; i++) alphabet += '[' + i;
				for (int i = 0; i < 4; i++) alphabet += '{' + i;
				check[3] = true;
			}
			break;
		default:
			alphabet = "";
			return;
		}
		j++;
	}
}

//exec loop input processing
unsigned char* getNextString(unsigned char *prev, int size, string &alphabet) {
	unsigned char *next = new unsigned char[size];
	if (prev == NULL) {
		for (int i = 0; i < size; i++) next[i] = alphabet[0];
		return next;
	}
	if (prev[size - 1] != alphabet[alphabet.length() - 1]) {
		for (int i = 0; i < size - 1; i++) next[i] = prev[i];
		int nextChar = alphabet.find(prev[size - 1]);
		nextChar++;
		next[size - 1] = alphabet[nextChar];
	}
	else {
		unsigned char *tmp = new unsigned char[size - 1];
		for (int i = 0; i < size - 1; i++) tmp[i] = prev[i];
		tmp = getNextString(tmp, size - 1, alphabet);
		for (int i = 0; i < size - 1; i++) next[i] = tmp[i];
		next[size - 1] = alphabet[0];
		delete(tmp);
	}
	return next;
}
vector<int> determineBaseSizes(int inputLength) {
	//minimal inputLength = 6
	vector<int> division;
	int length = inputLength - 5; // 2 chars dedicated for kernels, 2 for devs, 1 for plats.
	division.push_back(length);
	division.push_back(1);
	division.push_back(2);
	return division;
}
unsigned char* getLastString(int size, string &alphabet) {
	unsigned char *result = new unsigned char[size];
	for (int i = 0; i < size; i++) result[i] = alphabet[alphabet.length() - 1];
	return result;
}
bool equals(unsigned char *s1, unsigned char *s2, int size) {
	for (int i = 0; i < size; i++) {
		if (s1[i] != s2[i]) return false;
	}
	return true;
}
void recursiveLoop(string &base, int flag, string &alphabet, vector<string> &strings) {
	if (flag != 0) {
		for (int i = 0; i < alphabet.length(); i++) {
			unsigned char ch = alphabet[i];
			string str = base;
			str += ch;
			recursiveLoop(str, flag - 1, alphabet, strings);
		}
	}
	else strings.push_back(base);
}
const unsigned char** getStrings(int size, string &alphabet, int *resultSize) {
	string base = "";
	vector<string> strings;
	recursiveLoop(base, size, alphabet, strings);
	*resultSize = strings.size();
	unsigned char **result = new unsigned char*[*resultSize];
	for ( cl_long i = 0; i < *resultSize; i++) {
		result[i] = new unsigned char[size];
		for (int j = 0; j < size; j++) {
			result[i][j] = strings[i][j];
		}
	}
	return (const unsigned char**)result;
}
const unsigned char* getBufferInput(int size, string &alphabet, int *kernelConstTableSize) {
	const unsigned char **tmp2 = getStrings(size, alphabet, kernelConstTableSize);
	unsigned char *tmp = new unsigned char[*kernelConstTableSize * size];
	for (int i = 0; i < *kernelConstTableSize; i++) {
		for (int j = 0; j < size; j++) {
			tmp[2 * i + j] = tmp2[i][j];
		}
	}
	return (const unsigned char*)tmp;
}

#pragma endregion

int main(/*int argc, unsigned char **argv*/)
{
	//pozniej wyrzucic
#pragma region Temp
	//tmp
	int argc = 7;
	char *argv[] = {
		//program name
		"program",
		//expected
		"4208fcfecc24ad23e9a7fe50b6f473b32f0eddff3f69d14aafa24246bf4a2e08cb7f41c1b3a1b05299310dbc27e6de36072d54359fa9197d1d69818a7df6db65",
		//iters
		"1024",
		//init
		"0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000",
		//alph
		"1",
		//min
		"8",
		//max
		"8" };
	//////////////////////
#pragma endregion

#pragma region InputParameters
	cout << "Checking input parameters..." << endl;
	//input parameters check
	if (argc < 7) {
		cout << "Invalid input!" << endl;
		system("pause");
		return 1;
	}

	string              expectedOutputString,
		initVectorString,
		alphabet = "";
	int                 iterations,
		minLength,
		maxLength;
	 cl_ulong  *expectedOutput,
		*initVector,
		expectedOutputSize,
		initVectorSize;

	expectedOutputString = toLowerHexString(argv[1]);
	if (expectedOutputString == "") {
		cout << "Invalid expected output (not hex)!" << endl;
		system("pause");
		return 1;
	}
	expectedOutputSize = expectedOutputString.length() / 16;
	expectedOutput = toULLTab(expectedOutputString, expectedOutputSize);
	iterations = toInt(argv[2]);
	if (iterations == -1) {
		cout << "Invalid iterations number!" << endl;
		system("pause");
		return 1;
	}
	initVectorString = toLowerHexString(argv[3]);
	if (initVectorString == "") {
		cout << "Invalid initVector (not hex)!" << endl;
		system("pause");
		return 1;
	}
	initVectorSize = initVectorString.length() / 16;
	initVector = toULLTab(initVectorString, initVectorSize);
	defineAlphabet(alphabet, argv[4]);
	if (alphabet == "") {
		cout << "Invalid alphabet definition!" << endl;
		system("pause");
		return 1;
	}
	minLength = toInt(argv[5]);
	if (minLength == -1) {
		cout << "Invalid minLength number!" << endl;
		system("pause");
		return 1;
	}
	maxLength = toInt(argv[6]);
	if (maxLength == -1) {
		cout << "Invalid maxLength number!" << endl;
		system("pause");
		return 1;
	}
	if (minLength > maxLength) {
		cout << "Invalid lengths (min > max)!" << endl;
		system("pause");
		return 1;
	}

	cout << "Parameters valid." << endl;
#pragma endregion

#pragma region MainVariables
	fstream file;
	vector<string> kernelCode;
	size_t codeLines, *lineSize;
	const char **code;

	cl_int errCode;
	cl_uint platformsNo;
	cl_platform_id *platform;
	cl_uint *devicesNo;
	cl_device_id **device;
	cl_context **context;
	cl_command_queue **commandQ;
	cl_program **program;
	cl_kernel **kernel;
	cl_mem ***buffers;

	string foundPassword = "";
	int flag = 0;
#pragma endregion

#pragma region KernelCode
	cout << "Loading kernel code..." << endl;

	//read kernel code
	file.open("kernel.cl", ios::in);
	if (file.is_open()) {
		string s;
		while (getline(file, s)) {
			kernelCode.push_back(s);
		}
	}
	else {
		cout << "Couldn't open the file!" << endl;
		system("pause");
		return 1;
	}
	codeLines = kernelCode.size();
	code = new const char*[codeLines];
	lineSize = new size_t[codeLines];
	for (int i = 0; i < codeLines; i++) {
		code[i] = kernelCode[i].c_str();
		lineSize[i] = kernelCode[i].length();
	}

	cout << "Kernel code loaded." << endl;
#pragma endregion

#pragma region Structure
	//structure
	cout << "Preparing program structure..." << endl;
	//platformsNo
	errCode = clGetPlatformIDs(0, NULL, &platformsNo);
	platformsNo = 1;
	if (errCode != 0) {
		cout << "\tError! Shutting down." << endl;
		system("pause");
		return 1;
	}
	//platformsIDs
	platform = new cl_platform_id[platformsNo];
	errCode = clGetPlatformIDs(platformsNo, platform, NULL);
	if (errCode != 0) {
		cout << "\tError! Shutting down." << endl;
		system("pause");
		return 1;
	}
	//init tables
	devicesNo = new cl_uint[platformsNo];
	device = new cl_device_id*[platformsNo];
	context = new cl_context*[platformsNo];
	commandQ = new cl_command_queue*[platformsNo];
	program = new cl_program*[platformsNo];
	kernel = new cl_kernel*[platformsNo];
	buffers = new cl_mem**[platformsNo];
	//for each platform...
	for (int plat = 0; plat < platformsNo; plat++) {
		//deviceIDs
		errCode = clGetDeviceIDs(platform[plat], CL_DEVICE_TYPE_ALL, 0, NULL, &devicesNo[plat]);
		if (errCode != 0) {
			cout << "\tError! Shutting down." << endl;
			system("pause");
			return 1;
		}
		//tmp///////////
		/*****/devicesNo[plat] = 1;
		////////////////
		//devices
		device[plat] = new cl_device_id[devicesNo[plat]];
		errCode = clGetDeviceIDs(platform[plat], CL_DEVICE_TYPE_ALL, 1, device[plat], NULL);
		if (errCode != 0) {
			cout << "\tError! Shutting down." << endl;
			system("pause");
			return 1;
		}
		//init tables
		context[plat] = new cl_context[devicesNo[plat]];
		commandQ[plat] = new cl_command_queue[devicesNo[plat]];
		program[plat] = new cl_program[devicesNo[plat]];
		kernel[plat] = new cl_kernel[devicesNo[plat]];
		buffers[plat] = new cl_mem*[devicesNo[plat]];
		//for each device...
		for (int dev = 0; dev < devicesNo[plat]; dev++) {
			//context
			context[plat][dev] = clCreateContext(NULL, 1, &device[plat][dev], NULL, NULL, &errCode);
			if (errCode != 0) {
				cout << "\tError! Shutting down." << endl;
				system("pause");
				return 1;
			}
			//commandQ
			commandQ[plat][dev] = clCreateCommandQueue(context[plat][dev], device[plat][dev], 0, &errCode);
			if (errCode != 0) {
				cout << "\tError! Shutting down." << endl;
				system("pause");
				return 1;
			}
			//program
			program[plat][dev] = clCreateProgramWithSource(context[plat][dev], codeLines, code, lineSize, &errCode);
			if (errCode != 0) {
				cout << "\tError! Shutting down." << endl;
				system("pause");
				return 1;
			}
			errCode = clBuildProgram(program[plat][dev], 1, &device[plat][dev], NULL, NULL, NULL);
			if (errCode == -11) {
				unsigned char *result;
				size_t infoSize;
				clGetProgramBuildInfo(program[plat][dev], device[plat][dev], CL_PROGRAM_BUILD_LOG, 0, NULL, &infoSize);
				result = new unsigned char[infoSize];
				clGetProgramBuildInfo(program[plat][dev], device[plat][dev], CL_PROGRAM_BUILD_LOG, infoSize, result, NULL);
				cout << result << endl;
				system("pause");
				return 1;
			}
			if (errCode != 0) {
				cout << "\tError! Shutting down." << errCode << endl;
				system("pause");
				return 1;
			}
			//kernel
			kernel[plat][dev] = clCreateKernel(program[plat][dev], "concat_compare", &errCode);
			if (errCode != 0) {
				cout << "\tError! Shutting down." << errCode << endl;
				system("pause");
				return 1;
			}
		}
	}

	cout << "Program prepared." << endl;
	cout << "Executing..." << endl;

#pragma endregion

#pragma region Execution
	//data
	int kernelConstTableSize;
	const unsigned char *kernelConstTable = getBufferInput(2, alphabet, &kernelConstTableSize);
	int currentLength = minLength;
	string base = "";
	while (currentLength <= maxLength) {
		cout << "\tLength: " << currentLength << endl;
		vector<int> baseSize = determineBaseSizes(currentLength);
		//global bedzie rozny i ogromny, wiec iterowany, reszta jest nieduza, wiec mozna zapamietac.
		int platformConstTableSize,
			deviceConstTableSize;
		const unsigned char **platformConstTable = getStrings(baseSize[1], alphabet, &platformConstTableSize),
			**deviceConstTable = getStrings(baseSize[2], alphabet, &deviceConstTableSize);
		unsigned char   *currentGlobalPart = NULL,
			*lastGlobalPart = getLastString(baseSize[0], alphabet);
		do {
			unsigned char *nextGlobalPart = getNextString(currentGlobalPart, baseSize[0], alphabet);
			delete(currentGlobalPart);
			currentGlobalPart = nextGlobalPart;

			int plat = 0;
			for (int pl = 0; pl < platformConstTableSize; pl++) {
				int dev = 0;
				for (int d = 0; d < deviceConstTableSize; d++) {

					//buffers
					buffers[plat][dev] = new cl_mem[9];

					//expectedOutput
					buffers[plat][dev][0] = clCreateBuffer(context[plat][dev], CL_MEM_READ_ONLY, expectedOutputSize * sizeof( cl_ulong), NULL, &errCode);

					//initVector
					buffers[plat][dev][1] = clCreateBuffer(context[plat][dev], CL_MEM_READ_ONLY, initVectorSize * sizeof( cl_ulong), NULL, &errCode);

					//kernelConstTable
					buffers[plat][dev][2] = clCreateBuffer(context[plat][dev], CL_MEM_READ_ONLY, kernelConstTableSize * sizeof(char), NULL, &errCode);

					//globalBase
					buffers[plat][dev][3] = clCreateBuffer(context[plat][dev], CL_MEM_READ_ONLY, baseSize[0] * sizeof(char), NULL, &errCode);

					//platformBase
					buffers[plat][dev][4] = clCreateBuffer(context[plat][dev], CL_MEM_READ_ONLY, baseSize[1] * sizeof(char), NULL, &errCode);

					//deviceBase
					buffers[plat][dev][5] = clCreateBuffer(context[plat][dev], CL_MEM_READ_ONLY, baseSize[2] * sizeof(char), NULL, &errCode);

					//k[] for sha
					buffers[plat][dev][6] = clCreateBuffer(context[plat][dev], CL_MEM_READ_ONLY, 80 * sizeof( cl_ulong), NULL, &errCode);

					//output
					buffers[plat][dev][7] = clCreateBuffer(context[plat][dev], CL_MEM_WRITE_ONLY, alphabet.length() * alphabet.length() * sizeof(int), NULL, &errCode);

					//output
					buffers[plat][dev][8] = clCreateBuffer(context[plat][dev], CL_MEM_WRITE_ONLY, currentLength * alphabet.length() * alphabet.length() * sizeof(int), NULL, &errCode);


					//tables into buffers
					errCode = clEnqueueWriteBuffer(commandQ[plat][dev], buffers[plat][dev][0], CL_TRUE, 0, expectedOutputSize * sizeof( cl_ulong), expectedOutput, 0, NULL, NULL);

					errCode = clEnqueueWriteBuffer(commandQ[plat][dev], buffers[plat][dev][1], CL_TRUE, 0, initVectorSize * sizeof( cl_ulong), initVector, 0, NULL, NULL);

					errCode = clEnqueueWriteBuffer(commandQ[plat][dev], buffers[plat][dev][2], CL_TRUE, 0, kernelConstTableSize * sizeof(char), kernelConstTable, 0, NULL, NULL);

					errCode = clEnqueueWriteBuffer(commandQ[plat][dev], buffers[plat][dev][3], CL_TRUE, 0, baseSize[0] * sizeof(char), currentGlobalPart, 0, NULL, NULL);

					errCode = clEnqueueWriteBuffer(commandQ[plat][dev], buffers[plat][dev][4], CL_TRUE, 0, baseSize[1] * sizeof(char), platformConstTable[pl], 0, NULL, NULL);

					errCode = clEnqueueWriteBuffer(commandQ[plat][dev], buffers[plat][dev][5], CL_TRUE, 0, baseSize[2] * sizeof(char), deviceConstTable[d], 0, NULL, NULL);

					errCode = clEnqueueWriteBuffer(commandQ[plat][dev], buffers[plat][dev][6], CL_TRUE, 0, 80 * sizeof( cl_ulong), k, 0, NULL, NULL);




					//kernel args
					errCode = clSetKernelArg(kernel[plat][dev], 0, sizeof(int), &expectedOutputSize);

					errCode = clSetKernelArg(kernel[plat][dev], 1, sizeof(cl_mem), &buffers[plat][dev][0]);

					errCode = clSetKernelArg(kernel[plat][dev], 2, sizeof(int), &iterations);

					errCode = clSetKernelArg(kernel[plat][dev], 3, sizeof(cl_mem), &buffers[plat][dev][1]);

					errCode = clSetKernelArg(kernel[plat][dev], 4, sizeof(int), &initVectorSize);

					errCode = clSetKernelArg(kernel[plat][dev], 5, sizeof(cl_mem), &buffers[plat][dev][2]);

					errCode = clSetKernelArg(kernel[plat][dev], 6, sizeof(int), &kernelConstTableSize);

					errCode = clSetKernelArg(kernel[plat][dev], 7, sizeof(cl_mem), &buffers[plat][dev][3]);

					errCode = clSetKernelArg(kernel[plat][dev], 8, sizeof(int), &baseSize[0]);

					errCode = clSetKernelArg(kernel[plat][dev], 9, sizeof(cl_mem), &buffers[plat][dev][4]);

					errCode = clSetKernelArg(kernel[plat][dev], 10, sizeof(int), &baseSize[1]);

					errCode = clSetKernelArg(kernel[plat][dev], 11, sizeof(cl_mem), &buffers[plat][dev][5]);

					errCode = clSetKernelArg(kernel[plat][dev], 12, sizeof(int), &baseSize[2]);

					errCode = clSetKernelArg(kernel[plat][dev], 13, sizeof(cl_mem), &buffers[plat][dev][6]);

					errCode = clSetKernelArg(kernel[plat][dev], 14, sizeof(int), &currentLength);

					errCode = clSetKernelArg(kernel[plat][dev], 15, sizeof(cl_mem), &buffers[plat][dev][7]);

					errCode = clSetKernelArg(kernel[plat][dev], 16, sizeof(cl_mem), &buffers[plat][dev][8]);


					size_t  globalIDs = alphabet.length() * alphabet.length(),
						localIDs = alphabet.length();

					errCode = clEnqueueNDRangeKernel(commandQ[plat][dev], kernel[plat][dev], 1, NULL, &globalIDs, &localIDs, 0, NULL, NULL);


					int *result = new int[globalIDs];

					errCode = clEnqueueReadBuffer(commandQ[plat][dev], buffers[plat][dev][7], CL_TRUE, 0, globalIDs * sizeof(int), result, 0, NULL, NULL);

					for (int i = 0; i < globalIDs; i++) flag |= result[i];
					if (flag) {
						unsigned char *passwords = new unsigned char[currentLength * globalIDs];
						errCode = clEnqueueReadBuffer(commandQ[plat][dev], buffers[plat][dev][8], CL_TRUE, 0, currentLength * globalIDs * sizeof(char), passwords, 0, NULL, NULL);
						for (int i = 0; i < globalIDs; i++) {
							if (result[i] == 1) {
								int offset = currentLength * i;
								for (int j = 0; j < currentLength; j++) foundPassword += passwords[offset + j];
								goto passwordFound;
							}
						}
					}


					dev++;
					if (dev == devicesNo[plat]) dev = 0;

					//local clean-up
					delete(result);
					for (int i = 0; i < 9; i++) clReleaseMemObject(buffers[plat][dev][i]);
					delete(buffers[plat][dev]);
				}
				plat++;
				if (plat == platformsNo) plat = 0;
			}
		} while (!equals(currentGlobalPart, lastGlobalPart, baseSize[0]));

		cout << endl << "\t\tNot found." << endl;
		currentLength++;

		//local clean-up
		for (int i = 0; i < platformConstTableSize; i++) delete(platformConstTable[i]);
		delete(platformConstTable);
		for (int i = 0; i < deviceConstTableSize; i++) delete(deviceConstTable[i]);
		delete(deviceConstTable);
		delete(currentGlobalPart);
		delete(lastGlobalPart);
	}
passwordFound: if (flag == 1) cout << "Found password: " << foundPassword << endl;
#pragma endregion

#pragma region CleaningUp
	cout << "Program ended. Cleaning up." << endl;
	delete(kernelConstTable);
	//clean-up
	for (int plat = 0; plat < platformsNo; plat++) {
		for (int dev = 0; dev < devicesNo[plat]; dev++) {
			errCode = clFlush(commandQ[plat][dev]);
			errCode = clFinish(commandQ[plat][dev]);
			errCode = clReleaseKernel(kernel[plat][dev]);
			errCode = clReleaseProgram(program[plat][dev]);
			errCode = clReleaseCommandQueue(commandQ[plat][dev]);
			errCode = clReleaseContext(context[plat][dev]);
			errCode = clReleaseDevice(device[plat][dev]);
		}
		delete(commandQ[plat]);
		delete(kernel[plat]);
		delete(program[plat]);
		delete(context[plat]);
		delete(device[plat]);
		delete(buffers[plat]);
	}
	delete(commandQ);
	delete(kernel);
	delete(program);
	delete(context);
	delete(device);
	delete(platform);
	delete(buffers);

	cout << "Done." << endl;
	cout << endl;
	system("pause");
	return 1;
#pragma endregion

}